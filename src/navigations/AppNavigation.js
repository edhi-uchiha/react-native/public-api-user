import React from 'react';
import { NavigationContainer } from "@react-navigation/native";
import AlbumScreen from "../screens/albums/AlbumScreen";
import SettingScreen from "../screens/settings/SettingScreen";
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import TabIcon from 'react-native-vector-icons/Feather';
import HomeScreen from '../screens/home/HomeScreen';

const Tab = createBottomTabNavigator();

const AppNavigation = () => <NavigationContainer>
    <Tab.Navigator
        tabBarOptions={{
            activeTintColor: 'green',
            inactiveTintColor: 'black',
            allowFontScaling: true,
            labelStyle: {
                marginBottom: 4,
                fontSize: 11
            },
            iconStyle: {
                marginTop: 4
            }
        }}>
        <Tab.Screen
            name="Home"
            component={HomeScreen}
            options={{
                tabBarLabel: 'Home',
                tabBarIcon: ({ color }) => <TabIcon name="home" color={color} size={24} />
            }}
        />
        <Tab.Screen
            name="Users"
            component={AlbumScreen}
            options={{
                tabBarLabel: 'Users',
                tabBarVisible: false,
                tabBarIcon: ({ color }) => <TabIcon name="users" color={color} size={24} />
            }}
        />
        <Tab.Screen
            name="Settings"
            component={SettingScreen}
            options={{
                tabBarLabel: 'Settings',
                tabBarIcon: ({ color }) => <TabIcon 
                name="settings" color={color} size={24} />,
            }} />
    </Tab.Navigator>
</NavigationContainer>

export default AppNavigation;