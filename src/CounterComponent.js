import React from 'react'
import { Button, StyleSheet, Text, TouchableOpacity, View } from 'react-native'

const CounterComponent = ({handleIncrement, handleDecrement, counter}) => {
    return (<View style={styles.container}>
    <TouchableOpacity style={styles.btn}
    onPress={handleIncrement}
    >
        <Text style={styles.text}>+</Text>
    </TouchableOpacity>
    <Text style={{ fontSize: 36, marginHorizontal: 24 }}>{counter}</Text>
    <TouchableOpacity style={styles.btn}
    onPress={handleDecrement}>
        <Text style={styles.text}>-</Text>
    </TouchableOpacity>

</View>)}

export default CounterComponent;

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center'
    },
    btn: {
        padding: 12,
        width: 64,
        backgroundColor: '#000',
        alignItems: 'center',
        borderRadius: 8
    },
    text: { color: 'white', fontSize: 30 }
})