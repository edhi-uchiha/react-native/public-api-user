import React from 'react';
import { View, Text, FlatList } from 'react-native';
import CounterComponent from './CounterComponent';

export default class CounterContainer extends React.Component {

    constructor() {
        super();
        this.state = {
            counter: 0,
            products: [
                {
                    id: 1212,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1213,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1214,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1212,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1213,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1214,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1215,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1216,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1217,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1218,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1219,
                    name: 'Keyboard',
                    price: 1200000
                },
                {
                    id: 1220,
                    name: 'Keyboard',
                    price: 1200000
                }
            ]
        }
    }

    handleIncrement = () => {
        this.setState({
            counter: this.state.counter + 1
        })
    }

    handleDecrement = () => {
        this.setState({
            counter: this.state.counter - 1
        })
    }

    render() {

        return <View style={{
            flex: 1,
            flexDirection: 'column',
            justifyContent: 'center',
            alignItems: 'center'
        }}>
            <Text style={{
                fontSize: 24
            }}>Counter App</Text>
            <CounterComponent
                counter={this.state.counter}
                handleIncrement={this.handleIncrement}
                handleDecrement={this.handleDecrement}
            />

        </View>
    }

}