const APP_TOKEN = 'ed85967e2c5a5049715470ceb936d02806ec459ff0d8bb38057ff0a145c37d95'
const REQUEST_HEADERS = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'Authorization' :  `Bearer ${APP_TOKEN}`
}

export { APP_TOKEN, REQUEST_HEADERS };