import React from 'react';
import { View, Text } from "react-native";
import MyHeader from '../../components/MyHeader';

const SettingScreen = (props) => {
    return (
        <>
            <MyHeader title={'Settings'} leftIcon={null} />
            <View style={{flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}>
                <Text>Setting Screen</Text>
            </View>
        </>
    );
}

export default SettingScreen;