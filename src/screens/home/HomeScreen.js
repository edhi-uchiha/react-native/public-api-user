import React from 'react';
import { View, Text } from "react-native";
import MyHeader from '../../components/MyHeader';

const HomeScreen = (props) => {
    return (
        <>
            <MyHeader title={'Home'} leftIcon={null} />
            <View style={{flex: 1, justifyContent: 'center', alignContent: 'center', alignItems: 'center'}}>
                <Text>Home Screen</Text>
            </View>
        </>
    );
}

export default HomeScreen;