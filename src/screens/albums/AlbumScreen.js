import React from 'react';
import { useState, useEffect } from 'react';
import { View, FlatList, StyleSheet, Modal, TextInput, Button, ToastAndroid, Text } from "react-native";
import MyHeader from '../../components/MyHeader';
import SearchComponent from '../../components/SearchComponent';
import AlbumCardCompoent from './AlbumCardComponent';
import { getUsers, createUser, editUser, deleteUser } from './UserService';
import Icon from 'react-native-vector-icons/Feather';
import VectorIcon from 'react-native-vector-icons/Ionicons';

const initialForm = {
    id: "",
    name: "",
    gender: "",
    status: "",
    email: ""
}

const AlbumScreen = (props) => {

    const [users, setUsers] = useState([])
    const [modalVisible, setModalVisible] = useState(false)
    const [form, setForm] = useState(initialForm)

    const handleChangeTextInput = (name, e) => {
        setForm({ ...form, [name]: e })
    }

    const handleSave = (action) => {
        switch (action) {
            case 'CREATE': {
                createUser({ id, ...form }).then(resp => {
                    if (resp.code == 201) {
                        showToast(`User created with ID ${resp.data.id}`)
                        setModalVisible(false)
                        setForm(initialForm)
                        loadData()

                    } else {
                        showToast(`${resp.data[0].field} ${resp.data[0].message}`)
                    }
                })
            }

                break;

            case 'EDIT': {
                editUser(form).then(resp => {
                    if (resp.code == 200) {
                        showToast(`User updated with ID ${resp.data.id}`)
                        setModalVisible(false)
                        setForm(initialForm)
                        loadData()
                    } else {
                        showToast(`${resp.data[0].field} ${resp.data[0].message}`)
                    }
                })
            }

                break;

            default:
                break;
        }
    }

    const handleDelete = (id) => {
        deleteUser(id).then(resp => {
            if (resp.code == 204) {
                showToast(`User ${id} deleted`)
                setModalVisible(false)
                setForm(initialForm)
                loadData()
            } else if (resp.code == 404) {
                showToast(`User with ${id} not found`)
            }
        })
    }

    const showToast = (message) => {
        ToastAndroid.show(message, ToastAndroid.SHORT);
    }

    const onEditClicked = (data) => {
        setForm(data)
        setModalVisible(true)
    }

    const loadData = () => {
        getUsers().then(resp => {
            setUsers(resp.data)
        })
    }

    useEffect(() => {
        loadData();
    }, [])

    return (
        <>
            <MyHeader title={'Users'} leftIcon={'arrow-left'} onClickedIcon={ () => props.navigation.navigate('Home')}/>
            <SearchComponent placeholder={'Search User . . .'} sortTitle={'Filter'} />
            <View style={styles.container}>
                <FlatList
                    data={users}
                    renderItem={({ item }) => <AlbumCardCompoent data={item} onEditClicked={onEditClicked} onDeleteClicked={handleDelete} />}
                    keyExtractor={(item) => item.id}
                />
            </View>
            <VectorIcon name='add-circle' size={66} style={{ position: 'absolute', bottom: 10, right: 10 }} color={'green'}
                onPress={() => setModalVisible(!modalVisible)} />
            <Modal
                visible={modalVisible}
                animationType='fade'
                presentationStyle='overFullScreen'
            >
                <View style={styles.centeredModal}>
                    <View style={styles.modalView}>
                        <View style={styles.title}>
                            <Text style={styles.modalTitle}>New User</Text>
                            <Icon
                                name='x'
                                size={24}
                                onPress={() => setModalVisible(false)}
                            />
                        </View>

                        <TextInput
                            value={form.name}
                            placeholder="Name"
                            onChangeText={(e) => {
                                handleChangeTextInput('name', e)
                            }}
                        />
                        <TextInput
                            value={form.gender}
                            placeholder="Gender"
                            onChangeText={(e) => {
                                handleChangeTextInput('gender', e)
                            }}
                        />
                        <TextInput
                            value={form.email}
                            placeholder="email"
                            onChangeText={(e) => {
                                handleChangeTextInput('email', e)
                            }}
                        />
                        <TextInput
                            value={form.status}
                            placeholder="status"
                            onChangeText={(e) => {
                                handleChangeTextInput('status', e)
                            }}
                        />
                        <Button
                            onPress={() => {
                                if (!form.id) {
                                    handleSave('CREATE')
                                } else {
                                    handleSave('EDIT')
                                }
                            }}
                            title="Save" />
                    </View>
                </View>
            </Modal>
        </>
    );
}

export default AlbumScreen;

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    centeredModal: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: 'rgba(0, 0, 0, 0.2)'
    },
    modalView: {
        backgroundColor: "#FFF",
        borderRadius: 8,
        padding: 16,
        margin: 16,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5
    },
    title: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },


})